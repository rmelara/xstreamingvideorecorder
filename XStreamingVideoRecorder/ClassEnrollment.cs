//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XStreamingVideoRecorder
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClassEnrollment
    {
        public int ClassEnrollmentId { get; set; }
        public int UserId { get; set; }
        public int ClassId { get; set; }
        public bool Enrolled { get; set; }
        public int MessageCount { get; set; }
    
        public virtual Class Class { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}
