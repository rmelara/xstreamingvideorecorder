//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XStreamingVideoRecorder
{
    using System;
    using System.Collections.Generic;
    
    public partial class Trainer
    {
        public Trainer()
        {
            this.Classes = new HashSet<Class>();
            this.FavoriteMessages = new HashSet<FavoriteMessage>();
            this.Sessions = new HashSet<Session>();
            this.UserProfiles = new HashSet<UserProfile>();
        }
    
        public int TrainerId { get; set; }
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Statement { get; set; }
        public byte[] TrainerImage { get; set; }
        public byte[] Thumbnail { get; set; }
        public int VideoAssetId { get; set; }
        public bool permissionPostFacebook { get; set; }
        public Nullable<int> Studio_StudioId { get; set; }
    
        public virtual ICollection<Class> Classes { get; set; }
        public virtual ICollection<FavoriteMessage> FavoriteMessages { get; set; }
        public virtual ICollection<Session> Sessions { get; set; }
        public virtual Studio Studio { get; set; }
        public virtual ICollection<UserProfile> UserProfiles { get; set; }
    }
}
