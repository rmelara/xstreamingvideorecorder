﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XStreamingVideoRecorder {
    public static class Helpers {
        public static Session nextSession;
        public static Boolean activeSession = false;
        public static Boolean incomingSession = false;
        public static Boolean countdown = false;
        public static Session currentSession;
        public static Boolean recording = false;
        public static Studio currentStudio;
        

        //Return true if exist a session in the next 15 minutes and if exist the session is saved
        public static bool searchNextSession() {
            try {
                DateTime Now = System.DateTime.Now;
                DateTime Limit = System.DateTime.Now.AddMinutes(10);
                
                using (var _db = new XStreamingFitnessTestEntities()) {
                    var session = _db.Sessions.Where(entry => entry.SessionDate >= Now && entry.SessionDate <= Limit).ToList();
                    if ((session.Count > 0) && (activeSession == false))
                    {
                        nextSession = session.First();
                        activeSession = true;
                        return true;
                    }
                    else if((session.Count > 0) && (activeSession == true))
                    {
                        currentSession = nextSession;
                        nextSession = session.First();
                        incomingSession = true;
                        return true;
                    }
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }

            return true;
        }
        
    }
}
