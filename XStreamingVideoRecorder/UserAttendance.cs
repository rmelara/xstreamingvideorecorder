//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XStreamingVideoRecorder
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserAttendance
    {
        public int UserAttendanceId { get; set; }
        public System.DateTime AttendedTime { get; set; }
        public string Description { get; set; }
        public double Credit { get; set; }
        public bool IsAttendable { get; set; }
        public Nullable<int> User_UserId { get; set; }
        public Nullable<int> Session_SessionId { get; set; }
    
        public virtual Session Session { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}
