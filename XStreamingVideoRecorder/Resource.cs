//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XStreamingVideoRecorder
{
    using System;
    using System.Collections.Generic;
    
    public partial class Resource
    {
        public int ResourceId { get; set; }
        public int ClassId { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public byte[] File { get; set; }
        public string FileName { get; set; }
        public string URL { get; set; }
        public int Views { get; set; }
        public System.DateTime CreatedDate { get; set; }
    }
}
