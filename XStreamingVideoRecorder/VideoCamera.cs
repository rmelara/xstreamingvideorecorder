﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using VimeoDotNet.Authorization;
using VimeoDotNet.Net;

namespace XStreamingVideoRecorder {
    class VideoCamera {
        #region
        private static string _baseUrl = "";
        private static readonly string _sdCard = "SD_DISK";
        private static readonly string _format = "matroska";
        //private static readonly string _recodingId = "20160331_110655_C044_ACCC8E46F3B7";
        private static string _startRecoringUrl = "";
        private static string _exportRecoringUrl = "";
        private static string _stopRecoringUrl = "";
        private static string _propertiesUrl = String.Format("{0}record/export/properties.cgi", _baseUrl);
        private static string _schemaUrl = String.Format("{0}disks/networkshare/schemaversions.cgi", _baseUrl);
        #endregion 
        #region "Recording Methods"
        public static string RecordingId;
        public static bool switchingCamera = false;
        public static bool videoStatus = false;
        public static string CameraPortRecording;
        public static string desCamera;
        public static string cameraLog = "";
        public static string pathString = "";
        public static string sessionTitle = "SessionService1D";
        public static List<Record> videoList = new List<Record>();
        public static List<string> pathList = new List<string>();
        public static List<string> mergeList = new List<string>();
        public static List<string> vimeoList = new List<string>();
       
        public Lazy<NetworkCredential> _axisCredential = new Lazy<NetworkCredential>();
        public NetworkCredential AxisCredential {
            get { return new NetworkCredential("root", "password11"); }
        }
        public bool StartRecording(string port, string camera) {
            try {
                pathList.Clear();
                videoStatus = true;
                CameraPortRecording = port;
                desCamera = camera;
                _baseUrl = "http://vpn.sincerasolutions.com:"+ port +"/axis-cgi/";
                _startRecoringUrl = String.Format("{0}record/record.cgi?diskid={1}", _baseUrl, _sdCard);
                

                var response = ExecuteRequest(_startRecoringUrl);
                XDocument doc;
                using (Stream responseStream = response.GetResponseStream()) {
                    doc = XDocument.Load(responseStream);
                }
                RecordingId = doc.Root.Element("record").Attribute("recordingid").Value;
                //videoList.Add(RecordingId);
                Program.insertRecord(RecordingId);
                cameraLog += "Video name: " + RecordingId + "-Camera: " + desCamera +"<br/>";
                return true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public bool StopRecording(int recordId) {
            try {
                _stopRecoringUrl = String.Format("{0}record/stop.cgi?recordingid=", _baseUrl);
                //RecordingId = "20160408_114704_CC1C_ACCC8E46F3B7";

                Record r = new Record();
                using (var _db = new XStreamingFitnessTestEntities())
                {
                    var crid = _db.Records.Find(recordId);
                    r = (Record)crid;
                }


                RecordingId = r.FullPath;
                var stopUrl = _stopRecoringUrl + RecordingId;
                var response = ExecuteRequest(stopUrl);
                response.Close();
                if (!switchingCamera) {
                    videoStatus = false;
                    cameraLog = "";
                }
                
                switchingCamera = false;
                return true;
            } catch (Exception ex) {
                return false;
            }
        }
        public async Task<bool> ExportRecording(String title="default")
        {
            if (_baseUrl == "")
            {
                int port = 4483;
                _baseUrl = "http://vpn.sincerasolutions.com:" + port + "/axis-cgi/";
            }
            try
            {
                

                using (var _db = new XStreamingFitnessTestEntities())
                {
                    videoList = (from r in _db.Records where r.Status_Id == 1 select r).ToList();
                    sessionTitle = videoList[0].Session.Title;
                }

                sessionTitle = sessionTitle.Replace(" ", "_");
                

                _exportRecoringUrl = String.Format("{0}record/export/exportrecording.cgi?schemaversion=1&recordingid={3}&diskid={1}&exportformat={2}", _baseUrl, _sdCard, _format, RecordingId);
                pathString = System.IO.Path.Combine(@"C:\Temp\", sessionTitle);
                System.IO.Directory.CreateDirectory(pathString);

                if (videoList.Count <= 0)
                {
                    Program.uploadStatus = true;
                    Program.downloadStatus = false;
                }
                //Download all the videos from the server
                foreach (Record item in videoList)
                {
                    
                    if (Program.downloadStatus && Program.SessionCompleted)
                    {
                        string pathSaveFile = "";
                        RecordingId = item.FullPath;
                        _exportRecoringUrl = String.Format("{0}record/export/exportrecording.cgi?schemaversion=1&recordingid={3}&diskid={1}&exportformat={2}", _baseUrl, _sdCard, _format, RecordingId);
                        string shortPath = sessionTitle.Trim() + "\file";
                         pathSaveFile = pathString + "\\file-" + item.FullPath + ".mkv";


                        string sourceUrl = _exportRecoringUrl;
                        string targetdownloadedFile = pathSaveFile;
                        TestDownload.DownloadManager downloadManager = new TestDownload.DownloadManager();
                        downloadManager.DownloadFile(sourceUrl, targetdownloadedFile);
                        pathList.Add(pathSaveFile);


                        Program.changeRecordStatus(item.Id, 4);
                        Program.insertRecordD(pathSaveFile);
                    }
                }

                
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool SwitchCamera(string newPort, string camera) {
            try {
                switchingCamera = true;
                if (!newPort.Equals(CameraPortRecording)) {
                    newPort = "4483";
                    //StopRecording();
                    return StartRecording(newPort,camera);
                }
                return false;
            } catch (Exception ex) {
                return false;
            }
        }
        public void updateStatus() {
            
        }
        public HttpWebResponse ExecuteRequest(string requestUrl) {
            WebRequest request = WebRequest.Create(requestUrl);
            request.Credentials = AxisCredential;
            return (HttpWebResponse)request.GetResponse();
        }
        public void convertVideos(String pathFile, String result) {
            try 
            {
                //String command = "-i C:\\a.mkv -f mpegts -c copy -bsf:v h264_mp4toannexb C:\\file-01.mpeg.ts ";

                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo.FileName = "ffmpeg";
                proc.StartInfo.Arguments = "-i "+pathFile+" -f mpegts -c copy -bsf:v h264_mp4toannexb " + result;
                proc.StartInfo.RedirectStandardError = true;
                proc.StartInfo.Verb = "runas";
                proc.StartInfo.UseShellExecute = false;
                if (!proc.Start()) {
                    System.Diagnostics.Debug.WriteLine("Error starting");
                    return;
                }
                StreamReader reader = proc.StandardError;
                string line;
                while ((line = reader.ReadLine()) != null) {
                    if(line.Equals(@"File 'C:\Temp\default\file-0.mpeg.ts' already exists. Overwrite ? [y/N] "))
                    {
                        Console.WriteLine("The file already exist.");
                        break;
                    }
                       
                    System.Diagnostics.Debug.WriteLine(line);
                }
                proc.Close();
            } catch (Exception ex) {
                System.Diagnostics.Debug.Write(ex.Message);
            }
            
        }


        public async Task<bool> stitchVideos(List<Record> pathList)
        {
            int n = 0;
            string name = pathList[0].Session.Title;
            string full = "";
            //Convert all the videos .mkv to .mpeg.ts 
            foreach (Record item in pathList)
            {
                System.Diagnostics.Debug.WriteLine(item.FullPath);
                convertVideos(item.FullPath, pathString + "\\file-" + n + ".mpeg.ts");
                mergeList.Add(pathString + "\\file-" + n + ".mpeg.ts");
                n++;
                Program.changeRecordStatus(item.Id, 4);
            }
            name = name.Replace(" ", "_");
            vimeoList.Add(pathString + "\\" + name + ".mkv");
            full = pathString + "\\" + name + ".mkv";
            mergeVideos(mergeList, pathString + "\\" + name + ".mkv");
            Program.insertRecordS(full, pathList[0].Session.SessionId);
            mergeList.Clear();
            pathList.Clear();
            updateStatus();
            
            return true;
        }


        public void mergeVideos(List<string> videoListM, String output) {
            try {
                output = output.Replace(" ", "_");
                //Merge all the .mpeg.ts videos
                string finishList = "";
                foreach (string item in videoListM) {
                    if (finishList.Equals("")) {
                        finishList += item;
                    } else {
                        finishList += "|" + item;
                    }

                }
                
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo.FileName = "ffmpeg";
                proc.StartInfo.Arguments = "-isync -i \"concat:" + finishList + "\" -f matroska -c copy " + output;
                proc.StartInfo.RedirectStandardError = true;
                proc.StartInfo.Verb = "runas";
                proc.StartInfo.UseShellExecute = false;
                if (!proc.Start()) {
                    System.Diagnostics.Debug.WriteLine("Error starting");
                    return;
                }
                StreamReader reader = proc.StandardError;
                string line;
                while ((line = reader.ReadLine()) != null) {
                    System.Diagnostics.Debug.WriteLine(line);
                }
                proc.Close();
            } catch (Exception ex) {
                System.Diagnostics.Debug.Write(ex.Message);
            }

        }
        public async Task<long> UploadToVimeo(String localPath)
        {
            var factory = new VimeoClientFactory();
            var vclient = factory.GetVimeoClient("7b3e8a86c63cca428bddf67b81f20f0d");
            
            Console.WriteLine("Starting upload"); 
             var file = new BinaryContent(localPath);
            file.OriginalFileName = "Test Upload";
            var response = vclient.UploadEntireFileAsync(file);
            Console.WriteLine("Upload started");
            await response;
            Console.WriteLine("Upload finished");
            var result = response.Result;


            Session SessionTemp = new Session();
            using (var _db = new XStreamingFitnessTestEntities())
            {
                SessionTemp = _db.Sessions.Find(Helpers.nextSession.SessionId);
                SessionTemp.VideoAssetId = Convert.ToInt32(result.Ticket.ticket_id);
                _db.SaveChanges();
                //sessionTitle = videoList[0].Session.Title;
            }



            Console.Out.WriteLine(result.Ticket.ticket_id);
            if (result.ClipId != null)
            {
                long id = result.ClipId ?? 0;
                file.Dispose();
                return id;
            }
            //Send other infomation to site (what information/where to); How often will this happen?
            file.Dispose();
            return 0;
        }
       
        #endregion
        public void clearList()
        {
            videoList.Clear();
        }
    }
}
