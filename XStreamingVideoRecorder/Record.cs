//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XStreamingVideoRecorder
{
    using System;
    using System.Collections.Generic;
    
    public partial class Record
    {
        public int Id { get; set; }
        public string FullPath { get; set; }
        public Nullable<int> Session_SessionId { get; set; }
        public Nullable<int> Status_Id { get; set; }
    
        public virtual RecordStatu RecordStatu { get; set; }
        public virtual Session Session { get; set; }
    }
}
