﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace XStreamingVideoRecorder {
    class Program {
        private static System.Timers.Timer aTimer;
        private static System.Timers.Timer bTimer;
        private static VideoCamera v = new VideoCamera();
        private static SessionLog currentSessionLog;
        private static DateTime partialTime;
        private static Boolean recordingFinished = false;
        private static int recordId = 0;

        private static List<Studio> studioList;
        private static List<Class> classList;
        private static Boolean multiRecording;


        public static Boolean downloadStatus = false;
        public static Boolean uploadStatus = false;
        public static Boolean recordStatus = false;
        public static Boolean stitchStatus = false;

        public static Boolean SessionCompleted = false;
        public static Boolean SessionDownload = false;

        //THREADS
        private static System.Threading.Thread record = new System.Threading.Thread(recordVideos);
        private static System.Threading.Thread download = new System.Threading.Thread(downloadVideos);
        private static System.Threading.Thread stitch = new System.Threading.Thread(stitchVideos);
        private static System.Threading.Thread upload = new System.Threading.Thread(uploadVideos);

        private static ManualResetEvent _record = new ManualResetEvent(true);
        
        
        private static void Main(string[] args) {
            studioList = new List<Studio>();
            classList = new List<Class>();

            Helpers.incomingSession = false;

            checkingStatus();

            
                download.Start();
                upload.Start();
                stitch.Start();
                record.Start();
            
            
        }






        #region PROCESS
        //RECORD PROCESS
        private static void SearchAndRecord(Object source, System.Timers.ElapsedEventArgs e)
        {
            checkingStatus();
            if (recordStatus)
            {
                try
                {
                    Console.WriteLine("Searching for a new session...");
                    Helpers.searchNextSession();


                    if (Helpers.activeSession)
                    {
                        Console.WriteLine("Found a session in the next 10 minutes...");
                        aTimer.Enabled = false;
                        DateTime t = Helpers.nextSession.SessionDate;
                        DateTime s = System.DateTime.Now;
                        TimeSpan waitingTime = t - s;
                        bTimer = new System.Timers.Timer();
                        Console.WriteLine(System.DateTime.Now);
                        System.Threading.Thread.Sleep(Convert.ToInt32(waitingTime.TotalMilliseconds));
                        Console.WriteLine("Recording...");
                        v.clearList();

                        SessionCompleted = false;
                        //Created the first partial time
                        //partialTime = DateTime.Now.AddMinutes(5);
                        partialTime = DateTime.Now.AddMilliseconds(30000);
                        Helpers.currentSession = Helpers.nextSession;
                        Helpers.nextSession = new Session();
                        v.StartRecording("4483", "service");

                        //Stop the other threads
                        //downloadStatus = false;


                        createSessionLog();
                        createClass(Helpers.currentSession.ClassId);
                        createStudio(Convert.ToInt32(classList[classList.Count - 1].Studio_StudioId));




                        //Recording duration in milliseconds
                        TimeSpan recordingDuration = Helpers.currentSession.SessionDate.AddMinutes(Helpers.currentSession.Duration) - Helpers.currentSession.SessionDate;

                        //We do a caller to the listener with the respective recording duration
                        listener(Convert.ToInt32(recordingDuration.TotalMilliseconds));
                        //listener(15000);

                        //We stop the current recording
                        stopSessionLog();
                        using (var _db = new XStreamingFitnessTestEntities())
                        {
                            recordId = _db.Records.OrderByDescending(p => p.Id).FirstOrDefault().Id;
                        }
                        v.StopRecording(recordId);


                        SessionCompleted = true;
                        Console.WriteLine("The recording has finished...");
                        Helpers.activeSession = false;
                        aTimer.Enabled = true;
                        recordingFinished = true;
                    }
                    else
                    {
                        //downloadStatus = true;
                        Console.WriteLine("Not found any session...");
                        Console.WriteLine("downloading...");

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error:" + ex.Message);
                }
            }
            else
            {
                Console.WriteLine("Record is not running");
            }


        }

        //DOWNLOAD PROCESS
        private static Boolean DownloadRecordings()
        {
            try {
                //we do an exportation for the recording
                    v.ExportRecording();
                    SessionDownload = true;
                    //We remove the studio and the class from the respective list
                    Class tmpClass = createClass(Helpers.currentSession.ClassId, true);
                    removeStudio(Convert.ToInt32(tmpClass.Studio_StudioId));
                    removeClass(tmpClass);
                return true;
            }
            catch(Exception e)
            {
                return false;
                Console.WriteLine("Error, download process:" + e.Message);
            }
        }

        //STITCH PROCESS
        private static void StitchRecordings()
        {

            using(var _db = new XStreamingFitnessTestEntities())
            {
                Record cr = new Record();
                var tempRecord = (
                                        from b in _db.Records
                                        where b.Status_Id == 2
                                        select b
                                        ).FirstOrDefault();
                cr = (Record)tempRecord;
                List<Record> videoList = new List<Record>();
                if (cr != null)
                {
                    
                    videoList = (from r in _db.Records where (r.Session_SessionId == cr.Session_SessionId && r.Status_Id == 2) select r).ToList();
                }


                if (videoList.Count > 0)
                {
                    v.stitchVideos(videoList);
                }
                
            }


            //v.stitchVideos();
        }

        //UPLOAD PROCESS
        private static void UploadRecordings()
        {
            try {
                List<Record> videoList = new List<Record>();
                using (var _db = new XStreamingFitnessTestEntities())
                {
                    videoList = (from r in _db.Records where r.Status_Id == 3 select r).ToList();
                }

                foreach(Record item in videoList)
                {
                    if (uploadStatus)
                    {
                        Console.WriteLine("Uploading video...");
                        Object d;
                        d= v.UploadToVimeo(item.FullPath);
                        Program.changeRecordStatus(item.Id, 4);
                        Program.insertRecordV("Test ID",Convert.ToInt32(item.Session_SessionId));
                    }
                }
                
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion


        public static void listener(int TotalMilliseconds){
            int countdown = TotalMilliseconds;
            while (countdown > 0) {

                if (!changeStatus()) {
                    countdown -= 1000;
                    System.Threading.Thread.Sleep(1000);
                }
                else
                {
                    return;
                }
            }
        }
        public static void createSessionLog() {
            try {
                Camera cameraDefault;
                RecordingStatu statusDefault;
                using (var _db = new XStreamingFitnessTestEntities()) {
                    //Recording status default
                    var recordingStatus = (
                                            from b in _db.RecordingStatus
                                            where b.RecordingStatusId == 2
                                            select b
                                            ).FirstOrDefault();
                    statusDefault = (RecordingStatu)recordingStatus;

                    //Camera default
                    var camera = (
                                            from b in _db.Cameras
                                            where b.CameraId == 1
                                            select b
                                            ).FirstOrDefault();
                    cameraDefault = (Camera)camera;


                    var session = (
                                        from b in _db.Sessions
                                        where b.SessionId == Helpers.currentSession.SessionId
                                        select b
                                        ).FirstOrDefault();


                    //Create the new session log with the default values
                    var sessionLog = new SessionLog {
                        Session = (Session)session,
                        Camera = cameraDefault,
                        RecordingStatu = statusDefault,
                    };

                    _db.SessionLogs.Add(sessionLog);
                    _db.SaveChanges();

                    //Get the last session log id to make the future requests
                    var csl = _db.SessionLogs.OrderByDescending(p => p.SessionLogId).FirstOrDefault();
                    currentSessionLog = (SessionLog)csl;
                }

              
            } catch (Exception ex) {
                Console.WriteLine("Error: " + ex.Message);
            }
        }
        //sltmp = session log temp
        //sc      = switch camera
        public static Boolean changeStatus() {
            try {
                Camera sc;
                SessionLog sessionLogTemp;

                //Search in the current session if the status has changed
                using (var _db = new XStreamingFitnessTestEntities()) {
                    var sltmp = (
                                       from b in _db.SessionLogs
                                       where b.SessionLogId == currentSessionLog.SessionLogId
                                       select b
                                       ).FirstOrDefault();
                    sessionLogTemp = (SessionLog)sltmp;
                }
                


                //Ask if the status is "recording" [ 2 = recording]
                if (sessionLogTemp.RecordingStatus_RecordingStatusId == 1) {
                    Console.WriteLine("Is not recording");
                } 
                else if (sessionLogTemp.RecordingStatus_RecordingStatusId == 2)
                //Ask if the status is "switch" to change the camera
                {
                    Console.WriteLine("recording");

                    //Ask if another session is coming
                    if (partialTime<=DateTime.Now)
                    {
                        Console.WriteLine("partial time - searching");
                        Helpers.searchNextSession();
                        if (Helpers.incomingSession)
                        {
                            //Class created with the current session
                            createClass(Helpers.nextSession.ClassId);
                            
                            Studio tempSt = (Studio)createStudio(Convert.ToInt32(classList[classList.Count - 1].Studio_StudioId),true);
                            
                            foreach(Studio s in studioList)
                            {
                                if (s.StudioId==tempSt.StudioId)
                                {
                                    Console.WriteLine("Is a new recording from the current studio");
                                    Console.WriteLine("The current recording will stop before the next session start, exactly 5 seconds before");
                                    TimeSpan wait = Helpers.nextSession.SessionDate - DateTime.Now;
                                    if(wait.TotalMilliseconds > 5000)
                                    {
                                        System.Threading.Thread.Sleep((Convert.ToInt32(wait.TotalMilliseconds) - 5000));
                                    }
                                    Helpers.incomingSession = false;
                                    return true;
                                }
                                else
                                {
                                    Console.WriteLine("Is a new recording from other studio");
                                    multiRecording = true;
                                }
                            }
                            
                            //Studio created with the current class
                            Helpers.incomingSession = false;
                            createStudio(Convert.ToInt32(classList[classList.Count - 1].Studio_StudioId));

                        }
                        else
                        {
                            //partialTime = DateTime.Now.AddMinutes(5);
                            partialTime = DateTime.Now.AddMilliseconds(30000);
                        }
                        
                    }
                }
                else if (sessionLogTemp.RecordingStatus_RecordingStatusId == 4)
                //Ask if the status is "switch" to change the camera
                { 
                    //Search the new camera
                    Console.WriteLine("Switch...");
                    using (var _db = new XStreamingFitnessTestEntities())
                    {
                        var camera = (
                                                from b in _db.Cameras
                                                where b.CameraId == sessionLogTemp.Camera_CameraId
                                                select b
                                                ).FirstOrDefault();
                        sc = (Camera)camera;


                        //Recording status default
                        var recordingStatus = (
                                                from b in _db.RecordingStatus
                                                where b.RecordingStatusId == 2
                                                select b
                                                ).FirstOrDefault();


                        SessionLog sl = _db.SessionLogs.First(i => i.SessionLogId == currentSessionLog.SessionLogId);
                        sl.RecordingStatu = (RecordingStatu)recordingStatus;
                        _db.SaveChanges();


                    }


                    //update the recording status



                    //v.StopRecording(recordId);
                    //v.StartRecording(sc.Port, sc.Model); //Start the a new recording
                }




                return false;
            } catch (Exception ex) {
                Console.WriteLine("Error: " + ex.Message);
                return false;
            }
            
        }
        public static void stopSessionLog()
        {
            using (var _db = new XStreamingFitnessTestEntities())
            {
                
                //Recording status default
                var recordingStatus = (
                                        from b in _db.RecordingStatus
                                        where b.RecordingStatusId == 3
                                        select b
                                        ).FirstOrDefault();


                SessionLog sl = _db.SessionLogs.First(i => i.SessionLogId == currentSessionLog.SessionLogId);
                sl.RecordingStatu = (RecordingStatu)recordingStatus;
                _db.SaveChanges();


            }
        }
        public static void createStudio(Int32 studioId)
        {
            try
            {
                using (var _db = new XStreamingFitnessTestEntities())
                {
                   studioList.Add(_db.Studios.Find(studioId));

                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
            }
            



        }
        public static Camera createCamera(Int32 cameraId)
        {
            try
            {
                using (var _db = new XStreamingFitnessTestEntities())
                {
                    return (Camera)_db.Cameras.Find(cameraId);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
                return null;
            }
        }
        public static Studio createStudio(Int32 studioId, Boolean rtn)
        {
            try
            {
                using (var _db = new XStreamingFitnessTestEntities())
                {
                    return (Studio)_db.Studios.Find(studioId);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
                return null;
            }




        }
        public static Class createClass(Int32 classId, Boolean rtn)
        {
            try
            {
                using (var _db = new XStreamingFitnessTestEntities())
                {
                    return (Class)_db.Classes.Find(classId);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
                return null;
            }
        }
        public static void createClass(Int32 classId)
        {
            try
            {
                using (var _db = new XStreamingFitnessTestEntities())
                {
                    classList.Add(_db.Classes.Find(classId));

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
            }




        }
        public static void createVideoAsset()
        {
            using (var _db = new XStreamingFitnessTestEntities())
            {
                //Create the new videoAsset
                var videoAsset = new VideoAsset
                {
                    Name = "",
                    Description = "",
                    BrightcoveId = 0,
                    VimeoId = 0,
                    VimeoThumbUrl = "",
                    VimeoUrlHd = "",
                    VimeoUrlSd = "",
                    VimeoUrlHls = "",
                    VimeoUrlMobile = "",
                    CreateDate = System.DateTime.Now,
                    DropDownListId = 0,
                    StudioId = 0,
                    Status = "",
                    Duration = 0,
                    Type = "",
                    DontDelete = false,
                    Studio_StudioId = 1,
                    Studio_StudioId1 = 1
                };

                _db.VideoAssets.Add(videoAsset);
                var lastVideoAsset = _db.VideoAssets.OrderByDescending(p => p.Id).FirstOrDefault();

                //attached it to session
                Session session = _db.Sessions.First(i => i.SessionId == currentSessionLog.SessionLogId);
                session.VideoAssetId = lastVideoAsset.Id;


                _db.SaveChanges();
            }
        }



        public static void removeStudio(Int32 studioId)
        {
            Studio temp = createStudio(studioId, true);
            studioList.Remove(temp);
        }
        public static void removeClass(Class tempclass)
        {
            classList.Remove(tempclass);
        }



        //THREADS
        public static void recordVideos()
        {
            Helpers.searchNextSession();
            Console.WriteLine("Press the Enter key to exit the program at any time... ");
            Console.WriteLine("Not Found a session in the next 15 minutes...");

            
                aTimer = new System.Timers.Timer(3000);
                aTimer.Elapsed += SearchAndRecord;
                aTimer.AutoReset = true;
                aTimer.Enabled = true;
                Console.ReadLine();
            
            
        }
        public static void downloadVideos()
        {
            while (true)
            {
                DownloadRecordings();
                System.Threading.Thread.Sleep(1000);
            }
        }
        public static void stitchVideos()
        {
            while (true)
            {
                if (stitchStatus && SessionDownload)
                {
                    StitchRecordings();
                    SessionDownload = false;
                    System.Threading.Thread.Sleep(1000);
                }
                
            }
        }
        public static void uploadVideos()
        {
            while (true)
            {
                if (uploadStatus)
                {
                    UploadRecordings();
                    System.Threading.Thread.Sleep(1000);
                }
            }
        }



        public static void insertRecord(String path)
        {
            using (var _db = new XStreamingFitnessTestEntities())
            {
                RecordStatu rs = _db.RecordStatus.Find(1);
                //Create the new session log with the default values
                var record = new Record
                {
                    FullPath = path,
                    Session_SessionId = Helpers.currentSession.SessionId,
                    RecordStatu=rs
                };

                try {
                    recordId = _db.Records.OrderByDescending(p => p.Id).FirstOrDefault().Id + 1;
                }
                catch(Exception ex)
                {
                    recordId =  1;
                }
                

                _db.Records.Add(record);
                _db.SaveChanges();


            }
                
        }
        public static void insertRecordD(String path)
        {
            using (var _db = new XStreamingFitnessTestEntities())
            {
                RecordStatu rs = _db.RecordStatus.Find(2);
                //Create the new session log with the default values
                var record = new Record
                {
                    FullPath = path,
                    Session_SessionId = Helpers.currentSession.SessionId,
                    RecordStatu = rs
                };

                recordId = _db.Records.OrderByDescending(p => p.Id).FirstOrDefault().Id + 1;

                _db.Records.Add(record);
                _db.SaveChanges();


            }

        }
        public static void insertRecordS(String path, int sessionId)
        {
            using (var _db = new XStreamingFitnessTestEntities())
            {
                RecordStatu rs = _db.RecordStatus.Find(3);
                //Create the new session log with the default values
                var record = new Record
                {
                    FullPath = path,
                    Session_SessionId = sessionId,
                    RecordStatu = rs
                };

                recordId = _db.Records.OrderByDescending(p => p.Id).FirstOrDefault().Id + 1;

                _db.Records.Add(record);
                _db.SaveChanges();


            }

        }
        public static void insertRecordV(String path, int sessionId)
        {
            using (var _db = new XStreamingFitnessTestEntities())
            {
                RecordStatu rs = _db.RecordStatus.Find(4);
                //Create the new session log with the default values
                var record = new Record
                {
                    FullPath = path,
                    Session_SessionId = sessionId,
                    RecordStatu = rs
                };

                recordId = _db.Records.OrderByDescending(p => p.Id).FirstOrDefault().Id + 1;

                _db.Records.Add(record);
                _db.SaveChanges();


            }

        }
        public static void changeRecordStatus(int RecordId, int Status)
        {
            using (var _db = new XStreamingFitnessTestEntities())
            {
                //Record cr = new Record();
                //var tempRecord = (
                //                        from b in _db.Records
                //                        where b.Id == RecordId
                //                        select b
                //                        ).FirstOrDefault();
                //cr = (Record)tempRecord;
                int newStatus = 0;
                //if (cr.Status_Id == 1) {
                //    newStatus = 2;
                //}
                //else if (cr.Status_Id == 2) {
                //    newStatus = 3;
                //}
                //else if (cr.Status_Id == 3) {
                //    newStatus = 4;
                //}
                //else if (cr.Status_Id == 4) {

                //    return;
                //}
                
                Record r = _db.Records.First(i => i.Id == RecordId);
                r.Status_Id = Status;
                _db.SaveChanges();


            }
        }
        public static void changeServiceStatus(int ThreadStatus)
        {
            using (var _db = new XStreamingFitnessTestEntities())
            {
                Thread cr = new Thread();
                
                Thread t = _db.Threads.First(i => i.Id == ThreadStatus);
                if(t.Working == 1)
                {
                    Console.WriteLine("The thread is running.");
                }
                else if(t.Working == 0)
                {
                    t.Working = 1;
                }

                if (ThreadStatus != 1)
                {
                    Thread t1 = _db.Threads.First(i => i.Id == 1);
                    t1.Working = 0;
                }

                if (ThreadStatus != 2)
                {
                    Thread t1 = _db.Threads.First(i => i.Id == 2);
                    t1.Working = 0;
                }

                if (ThreadStatus != 3)
                {
                    Thread t1 = _db.Threads.First(i => i.Id == 3);
                    t1.Working = 0;
                }

                _db.SaveChanges();


            }
        }

        public static void checkingStatus()
        {
            List<ServiceStatu> servicesList = new List<ServiceStatu>();
            using (var _db = new XStreamingFitnessTestEntities())
            {
                servicesList = (from r in _db.ServiceStatus select r).ToList();
            }

            //RECORD
            if (servicesList[0].State == 1)
            {
                recordStatus = true;
            }
            else
            {
                recordStatus = false;
            }

            //DOWNLOAD
            if (servicesList[1].State == 1)
            {
                downloadStatus = true;
            }
            else
            {
                downloadStatus = false;
            }


            //STITCH
            if (servicesList[2].State == 1)
            {
                stitchStatus = true;
            }
            else
            {
                stitchStatus = false;
            }

            //UPLOAD
            if (servicesList[3].State == 1)
            {
                uploadStatus = true;
            }
            else
            {
                uploadStatus = false;
            }
        }
    }
}
